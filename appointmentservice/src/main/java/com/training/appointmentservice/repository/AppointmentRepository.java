package com.training.appointmentservice.repository;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.appointmentservice.model.Appointment;

public interface AppointmentRepository extends JpaRepository<Appointment, Long> {
	Appointment getAppointmentByDoctorId(long doctorId);
	List <Appointment> getAllAppointmentByDoctorId(long doctorId);
	List <Appointment> deleteAllAppointmentByDoctorId(long doctorId);
}