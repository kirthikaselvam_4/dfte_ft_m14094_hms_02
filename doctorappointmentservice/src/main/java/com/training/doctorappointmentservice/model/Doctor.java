package com.training.doctorappointmentservice.model;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Doctor {
	
	
	private long doctorId;
	private String firstName;
	
	private String lastName;
	private String departmentName;
	private Appointment appointment;
	private List<Appointment> app;
	//specialist
	/*@ManyToOne(targetEntity = Hospital.class,cascade=CascadeType.ALL)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "refHospitalId", referencedColumnName = "hospitalId")
	private Hospital refHospitalId;*/
	
}