package com.training.doctorappointmentservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.training.doctorappointmentservice.model.Doctor;
import com.training.doctorappointmentservice.service.DoctorAppointmentService;




@RestController
public class DoctorAppointmentController {
	
	@Autowired
	DoctorAppointmentService docAppointmentService;
	
	@GetMapping("/doctors")
	public List<Doctor> getDoctors(){
		return docAppointmentService.getDoctors();
	}
	
	@PostMapping("/appointment")
	public Doctor createAddress(@RequestBody Doctor doctor) {
		return docAppointmentService.addDoctorAppointment(doctor);
	}
	@GetMapping(value = "doctor/{doctorId}")
    public Doctor getOneDoctor(@PathVariable long doctorId)
    {
		return docAppointmentService.getOneDoctor(doctorId);
    }
	@DeleteMapping(value = "/{doctorId}")
	public String deleteDoctorDetailsById(@PathVariable Long doctorId) {
		return docAppointmentService.deleteDoctorDetailsById(doctorId);
	}
	
}
