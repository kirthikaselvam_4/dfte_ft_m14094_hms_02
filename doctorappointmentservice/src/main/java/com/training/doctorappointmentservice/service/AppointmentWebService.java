package com.training.doctorappointmentservice.service;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.training.doctorappointmentservice.model.Appointment;

@FeignClient("APPOINTMENTSERVICE")
public interface AppointmentWebService {
	
	@GetMapping("/appointment/doc/{doctorId}")
	public Appointment getAppointmentByDoctorId(@PathVariable long doctorId);
	
	

	@PostMapping("/appointment")
	public Appointment addDoctor(@RequestBody Appointment appointment);
	
	@GetMapping("/appointments/doc/{doctorId}")
	public List<Appointment> getAllAppointmentByDoctorId(@PathVariable long doctorId);
}
