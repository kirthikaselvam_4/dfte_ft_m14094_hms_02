package com.training.doctorappointmentservice.service;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;

import com.training.doctorappointmentservice.model.Doctor;



public interface DoctorAppointmentService {

	//public List<Doctor> getDoctors();
	public Doctor addDoctorAppointment(Doctor doctor);
	
	public List<Doctor> getDoctors();
	public Doctor getOneDoctor(long doctorId);
	public String deleteDoctorDetailsById(@PathVariable long doctorId);
}
