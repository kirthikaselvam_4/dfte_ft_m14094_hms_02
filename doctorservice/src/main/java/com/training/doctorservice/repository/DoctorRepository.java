package com.training.doctorservice.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.training.doctorservice.model.Doctor;
@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Long> {

}