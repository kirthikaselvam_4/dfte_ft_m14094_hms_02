package com.training.doctorservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.training.doctorservice.model.Doctor;
import com.training.doctorservice.repository.DoctorRepository;

@Service
public class DoctorServiceImpl implements DoctorService {

	@Autowired
	DoctorRepository doctorRepository;

	@Override
	public List<Doctor> getDoctors() {
		return doctorRepository.findAll();
	}

	@Override
	public Doctor createDoctor(Doctor doctor) {
		// TODO Auto-generated method stub
		return doctorRepository.save(doctor);
	}
	/*
	 * @Override public List<Doctor> getDoc(long doctorId) { return
	 * doctorRepository.findAll(); }
	 */

	@Override
	public Doctor getOneDoctor(long doctorId) {
		// TODO Auto-generated method stub
		return doctorRepository.findById(doctorId).orElse(new Doctor());
	}

	@Override
	public boolean deleteDoctor(long doctorId) {
		// TODO Auto-generated method stub
		doctorRepository.deleteById(doctorId);
		return true;
	}
}

/*
 * @Override public Doctor getOneDoctor(Long doctorId) { // TODO Auto-generated
 * method stub return doctorRepository.findById(doctorId).orElse(new Doctor());
 * }
 * 
 * @Override public List<Doctor> getAllDoctorDetails() { // TODO Auto-generated
 * method stub return doctorRepository.findAll(); }
 * 
 * @Override public Doctor createDoctorDetails(@RequestBody Doctor doctor) { //
 * TODO Auto-generated method stub return doctorRepository.save(doctor); }
 * 
 * @Override public Doctor updateDoctorDetails(Doctor doctor) { // TODO
 * Auto-generated method stub return doctorRepository.save(doctor); }
 * 
 * 
 */